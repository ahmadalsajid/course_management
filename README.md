# Course Management
This is a demo project to show what I have learnt or how I work on python/Django projects. Also, I will keep some reference to other things i.e. GIT, RDBMS, etc.

## Prerequisites:
#### Environment setup (Linux)
Install mysql client before pip install on fresh machine
```
sudo apt-get install libmysqlclient-dev -y
sudo apt-get install python3.6-dev -y
```

## RabbitMQ
We will be using [RabbitMQ](https://www.rabbitmq.com/) as message broker for now. So, we will install it first.
```
sudo apt-get install -y erlang
sudo apt-get install rabbitmq-server -y
```
We have to enable and start RabbitMQ service.
```
sudo systemctl enable rabbitmq-server
sudo systemctl start rabbitmq-server
```
We can check whether it is running or not by executing this command.
```
systemctl status rabbitmq-server
```

#### Virtual environment setup
For creating virtual environment with python 3.6 in your machine type this
```
virtualenv -p python3.6 your_envname
```
Activate virtualenv
```
. path/to/virtualenv/your_envname/bin/activate
```
Install dependencies from requirements.txt
```
pip install -r requirements.txt
```

#### Run Application:
##### Linux
```
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```

## Custom Commands:
To create bulk user (i.e. teacher) , you can execute
```
python manage.py create_teachers 2
```
Also, if you want to add prefix in username, you can execute
```
python manage.py create_teachers 2 -p teacher
```
You can see the process [here](person/management/commands/create_teachers.py)

## Custom Manager:
You can easily add custom Manager to your model if necessary. For example, we 
can see that in our model, same model `Person` is used for teachers and 
student. So to find all the teachers from the `Person` model, we can use filter, i.e. 
```
Person.objects.filter(user_roll=2)
``` 
or, to find all the students
```
Person.objects.filter(user_roll=1)
```

Instead, we can have a custom Manager added to our model to easily get the data. i.e.
```
Person.people.teachers()
Person.people.students()
```
[Here](person/models.py), we have created a Custom Manager named `PersonManager`
 which has the functionality to filter teachers or students. We assigned it to 
 `Person` model with the variable `people`. as Django makes the first manager 
 as its default manager, to get the functionality of `objects` manager (which 
 is default in django, if no other custom manager is declared), we have to 
 assign it first before any other manager in the model.
 

## Unit Test: `Developing`
You can get the unit test codes from [here](person/tests).
To run unit test.
```
pytest
```
Running tests in parallel.
```
pytest -n <number of processes>
``` 

## Celery
Add `CELERY_BROKER_URL` to the `settings.py` file:
```
CELERY_BROKER_URL = 'amqp://localhost'
```
Now, create a file named [celery.py](course_management/celery.py) alongside the `settings.py` 
and `urls.py` files with the below contents.
```python
import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'course_management.settings')

app = Celery('course_management')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
```
Then, we have to update [`__init__.py`](course_management/__init__.py) file in the project root as follows.
```
from .celery import app as celery_app

__all__ = ['celery_app']
```
We will create a file named [`tasks.py`](person/tasks.py) inside any Django app(here, in `person` app) 
and put all our Celery tasks into this file. The Celery app we created in the project root will 
collect all tasks defined across all Django apps listed in the INSTALLED_APPS configuration.
Now, Create necessary [forms](person/forms.py), [views](person/views/person.py) and 
[urls](person/urls.py) to check whether it is working properly or not.

we will have to start the worker process. To do that, we will open an new terminal, and run 
the command `celery -A course_management worker -l info` where `course_management` is the 
project name. While checking the worker process, will will see the response if a task is submitted.
```
[2019-05-26 12:19:29,161: INFO/MainProcess] celery@VSL ready.
[2019-05-26 12:25:39,941: INFO/MainProcess] Received task: person.tasks.create_random_students[9795de87-90b3-4072-8f72-88f78c75e35f]  
[2019-05-26 12:28:02,153: INFO/ForkPoolWorker-2] Task person.tasks.create_random_students[9795de87-90b3-4072-8f72-88f78c75e35f] succeeded in 142.21024484600002s: '300 random users created with success!'
``` 

#### Celery work processor in production with SupervisorD
As we deploy out productions to AWS EC2 or DigitalOcean, we will run the worker process 
in the background. To do so, we will use SupervisorD. First we have to install it by using 
```
sudo apt-get install supervisord
``` 
After that, we have to create a file named `course_management-celery.conf` in the 
folder `/etc/supervisor/conf.d/course_management-celery.conf` with the below contents. 
Here, we assume that Django is installed in a virtualenv and the path to the virtualenv 
is `/home/course_management/`
```
[program:course_management-celery]
command=/home/course_management/bin/celery worker -A course_management --loglevel=INFO
directory=/home/course_management/course_management
user=nobody
numprocs=1
stdout_logfile=/home/course_management/logs/celery.log
stderr_logfile=/home/course_management/logs/celery.log
autostart=true
autorestart=true
startsecs=10

; Need to wait for currently executing tasks to finish at shutdown.
; Increase this if you have very long running tasks.
stopwaitsecs = 600

stopasgroup=true

; Set Celery priority higher than default (999)
; so, if rabbitmq is supervised, it will start first.
priority=1000
```
Now, we have to reload the configuration and add the new process by 
```
sudo supervisorctl reread
sudo supervisorctl update
```

References and tutorials for Celery and RabbitMQ are from 
[here](https://simpleisbetterthancomplex.com/tutorial/2017/08/20/how-to-use-celery-with-django.html), 
[here](http://docs.celeryproject.org/en/latest/django/first-steps-with-django.html#using-celery-with-django), 
and [here](https://www.rabbitmq.com/)