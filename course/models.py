from django.db import models
from person.models import Person


class Course(models.Model):
    course_name = models.CharField(max_length=300)
    course_code = models.CharField(max_length=20)
    description = models.TextField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.course_name) + ', ' + str(self.course_code)

    class Meta:
        db_table = 'courses'
        ordering = ('-created_at',)


semester_type_choices = (
    (1, 'Summer'),
    (2, 'Fall')
)


class Semester(models.Model):
    semester_type = models.PositiveSmallIntegerField(choices=semester_type_choices, default=1)
    start_date = models.DateField()
    end_date = models.DateField()
    adviser = models.ForeignKey(Person, related_name='advises',blank=True, null=True, on_delete=models.SET_NULL)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.get_semester_type_display()) + ', ' + self.start_date.strftime('%Y')

    class Meta:
        db_table = 'semesters'
        ordering = ('-created_at',)


class Class(models.Model):
    semester = models.ForeignKey(Semester, related_name='classes', on_delete=models.CASCADE)
    course = models.ForeignKey(Course, related_name='classes', null=True, on_delete=models.SET_NULL)
    teachers = models.ManyToManyField(Person, related_name='classes_as_teacher')
    students = models.ManyToManyField(Person, related_name='classes_as_student')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.semester) + ', ' + str(self.course.course_name)

    class Meta:
        db_table = 'classes'
        verbose_name_plural = 'Classes'
        ordering = ('-created_at',)
