from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from rangefilter.filter import DateRangeFilter
from django_admin_listfilter_dropdown.filters import DropdownFilter, RelatedDropdownFilter
from person.models import Person
from person.resources import PersonResource


@admin.register(Person)
class PersonAdmin(ImportExportModelAdmin):
    search_fields = ('user__username', 'user__email', 'user__first_name',
                     'user__last_name', 'mobile', 'identification_number')
    list_display = ('identification_number', 'get_username', 'user_roll',
                    'get_email', 'mobile', 'department')

    list_filter = (
        ('user', RelatedDropdownFilter),

        ('date_of_birth', DateRangeFilter),
        'department',
        'gender',
        'user_roll'
    )

    def get_username(self, obj):
        return obj.user.username

    get_username.short_description = 'Username'

    def get_email(self, obj):
        return obj.user.email

    get_email.short_description = 'Email'

    resource_class = PersonResource
