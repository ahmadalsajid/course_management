from django import forms
from person.models import Person
from django.core.validators import MinValueValidator, MaxValueValidator


class PersonForm(forms.ModelForm):
    email = forms.EmailField(required=True, widget=forms.EmailInput(
        attrs={'class': 'form-control form-group__input'}))
    password = forms.CharField(required=True, widget=forms.PasswordInput(
        attrs={'class': 'form-control form-group__input'}))
    first_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control form-group__input'}))
    last_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control form-group__input'}))

    class Meta:
        model = Person
        exclude = ('id', 'user', 'active', 'created_at', 'updated_at')
        widgets = {
            'mobile': forms.TextInput(
                attrs={'class': 'form-control form-group__input'}),
            'identification_number': forms.TextInput(
                attrs={'class': 'form-control form-group__input'}),
            'department': forms.Select(
                attrs={'class': 'form-control simple-select form-group__input'}),
            'date_of_birth': forms.TextInput(
                attrs={'class': 'form-control form-group__input daterange-input-single'}),
            'gender': forms.Select(
                attrs={'class': 'form-control simple-select form-group__input'}),
            'user_roll': forms.Select(
                attrs={'class': 'form-control simple-select form-group__input'}),
        }


class CreateRandomStudentForm(forms.Form):
    total = forms.IntegerField(validators=[MinValueValidator(50), MaxValueValidator(5000)])
