from random import randint
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string
from person.models import Person


class Command(BaseCommand):
    help = 'Create teachers'

    def add_arguments(self, parser):
        parser.add_argument('total', type=int, help='Indicates the number of teachers to be created')

        # Optional argument
        parser.add_argument('-p', '--prefix', type=str, help='Define a username prefix', )

    def handle(self, *args, **options):
        total = options.get('total')
        prefix = options.get('prefix')
        for i in range(total):
            random_string = get_random_string()
            if prefix:
                username = '{prefix}_{random_string}'.format(prefix=prefix, random_string=random_string)
            else:
                username = get_random_string()
            user = User.objects.create_user(username=username, email=random_string + '@mail.com', password='1qweqwe23')
            user.first_name = random_string
            user.last_name = random_string[::-1]
            user.save()
            teacher = Person()
            teacher.user = user
            # teacher id
            last_teacher = Person.objects.filter(user_roll=2).order_by('-created_at').first()
            sl_no = 0
            if last_teacher:
                sl_no = int(last_teacher.identification_number[1:])
            teacher.identification_number = 'T' + str(sl_no + 1).zfill(5)
            teacher.department = randint(2, 9)
            teacher.gender = randint(1, 2)
            teacher.user_roll = 2
            teacher.save()
        self.stdout.write('Total {} teachers created'.format(total))
