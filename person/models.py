from django.contrib.auth.models import User
from django.db import models

roll_choices = (
    (1, 'Student'),
    (2, 'Teacher'),
    (3, 'Admin')
)

gender_choices = (
    (1, 'Male'),
    (2, 'Female'),
    (3, 'Other'),
)

department_choices = (
    (1, 'N/A'),  # for admin
    (2, 'CSE'),
    (3, 'EEE'),
    (4, 'Civil'),
    (5, 'Architecture'),
    (6, 'DBA'),
    (7, 'Law'),
    (8, 'English'),
    (9, 'Pharmacy'),
)


class PersonQuerySet(models.QuerySet):
    def teachers(self):
        return self.filter(user_roll=2)

    def students(self):
        return self.filter(user_roll=1)


class PersonManager(models.Manager):
    def get_queryset(self):
        return PersonQuerySet(self.model, using=self._db)

    def teachers(self):
        return self.get_queryset().teachers()

    def students(self):
        return self.get_queryset().students()


class Person(models.Model):
    user = models.ForeignKey(User, related_name='person', on_delete=models.CASCADE)
    mobile = models.CharField(max_length=50, null=True, blank=True)
    identification_number = models.CharField(max_length=50, null=True, blank=True)
    department = models.PositiveSmallIntegerField(choices=department_choices, default=1)
    date_of_birth = models.DateField(null=True, blank=True)
    gender = models.PositiveSmallIntegerField(choices=gender_choices, default=1)
    user_roll = models.PositiveSmallIntegerField(choices=roll_choices, default=1)

    active = models.BooleanField(default=True)  # for soft delete

    # custom manager, as django treats the first manager as default manager,
    # we have to define the objects as the first manager to get the built-in functionality.
    objects = models.Manager()
    people = PersonManager()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.get_user_roll_display()) + ': ' + str(self.identification_number) \
               + ', username: ' + str(self.user.username)

    class Meta:
        db_table = 'persons'
        ordering = ('-created_at',)
