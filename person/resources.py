from import_export import resources
from person.models import Person


class PersonResource(resources.ModelResource):
    class Meta:
        model = Person
        skip_unchanged = True
        report_skipped = True
        exclude = ('id', 'created_at', 'updated_at')
