import string
from random import randint
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from celery import shared_task
from person.models import Person


@shared_task
def create_random_students(total=100):
    """
    This will create students randomly of total amount. If no `total`
    is provided, then it will create 100 students by default
    """
    for n in range(total):
        username = 'user_{}'.format(get_random_string(10, string.ascii_letters))
        email = '{}@example.com'.format(username)
        random_string = get_random_string(10)
        user = User.objects.create_user(username=username, email=email, password=random_string)
        user.first_name = random_string
        user.last_name = random_string[::-1]
        user.save()
        student = Person()
        student.user = user
        # student id
        last_student = Person.objects.filter(user_roll=1).order_by('-created_at').first()
        sl_no = 0
        if last_student:
            sl_no = int(last_student.identification_number[1:])
        student.identification_number = 'S' + str(sl_no + 1).zfill(5)
        student.department = randint(2, 9)
        student.gender = randint(1, 2)
        student.user_roll = 1
        student.save()
    return '{} random users created with success!'.format(total)
