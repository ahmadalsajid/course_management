import pytest

from mixer.backend.django import mixer


@pytest.mark.django_db
class TestModels:

    def test_person_is_teacher(self):
        person = mixer.blend('person.Person', user_roll=2)
        assert person.user_roll == 2

    def test_person_is_student(self):
        person = mixer.blend('person.Person', user_roll=1)
        assert person.user_roll == 1
