from django.urls import reverse, resolve


class TestUrls:
    def test_list_url(self):
        path = reverse('person:person_list')
        assert resolve(path).view_name == 'person:person_list'
