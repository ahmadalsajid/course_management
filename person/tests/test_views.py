from django.contrib.auth.models import User, AnonymousUser
from django.test import RequestFactory
from django.urls import reverse
from mixer.backend.django import mixer
from person.views import PersonListView
from django.test import TestCase
import pytest


@pytest.mark.django_db
class TestViews(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.factory = RequestFactory()

    def test_person_list_authenticated(self):
        path = reverse('person:person_list')
        request = self.factory.get(path)
        request.user = mixer.blend(User)

        response = PersonListView()
        assert response.get(request).status_code == 200

    def test_person_list_unauthenticated(self):
        path = reverse('person:person_list')
        request = self.factory.get(path)
        request.user = AnonymousUser()

        response = PersonListView().get(request)
        assert response.status_code == 302


