from django.urls import path
from person.views import PersonListView, PersonCreateView, CeleryBulkStudentCreateView

app_name = 'person'

urlpatterns = [
    path('', PersonListView.as_view(), name='person_list'),
    path('create/', PersonCreateView.as_view(), name='create_person'),
    path('bulk_student_create/', CeleryBulkStudentCreateView.as_view(), name='bulk_student_create'),
]
