from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import transaction
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
import logging
from django.conf import settings
from person.models import Person
from person.forms import PersonForm, CreateRandomStudentForm
from person.tasks import create_random_students

logger = logging.getLogger('course_management')


class PersonListView(View):
    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        page = request.GET.get('page', settings.DEFAULT_PAGE)
        per_page = request.GET.get('per_page', settings.PER_PAGE)

        try:
            persons = Person.objects.filter(active=True)
            paginator = Paginator(persons, per_page)

            try:
                persons = paginator.page(page)
            except PageNotAnInteger:
                persons = paginator.page(1)
            except EmptyPage:
                persons = paginator.page(paginator.num_pages)

            context = {
                'persons': persons,
            }
            return render(request, 'person/list.html', context)

        except Exception as e:
            logger.error(str(e))
            messages.error(request, str(e))
            return redirect('dashboard')


class PersonCreateView(View):
    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):

        try:
            form = PersonForm()
            context = {
                'form': form,
            }
            return render(request, 'person/create.html', context)

        except Exception as e:
            logger.error(str(e))
            messages.error(request, str(e))
            return redirect('dashboard')

    def post(self, request):

        try:
            form = PersonForm(request.POST or None)
            if form.is_valid():
                data = form.cleaned_data
                # create django User first
                first_name = data.get('first_name')
                last_name = data.get('last_name')
                email = data.get('email')
                password = data.get('password')
                user = User()
                user.username = email
                user.email = email
                user.set_password(password)
                user.first_name = first_name
                user.last_name = last_name
                user.save()  # user created
                # now create person
                mobile = data.get('mobile')
                identification_number = data.get('identification_number')
                department = data.get('department')
                date_of_birth = data.get('date_of_birth')
                gender = data.get('gender')
                user_roll = data.get('user_roll')
                person = Person()
                person.user = user
                person.mobile = mobile
                person.identification_number = identification_number
                person.department = department
                person.date_of_birth = date_of_birth
                person.gender = gender
                person.user_roll = user_roll
                person.save()

                messages.success(request, 'Successfully created User: {}'.format(person.user.username))
                return redirect('person:person_list')
            else:
                logger.error(str(form.errors))
                context = {
                    'form': form,
                }
                return render(request, 'person/create.html', context)

        except Exception as e:
            logger.error(str(e))
            messages.error(request, str(e))
            return redirect('dashboard')


class CeleryBulkStudentCreateView(View):
    @csrf_exempt
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        try:
            form = CreateRandomStudentForm()
            context = {
                'form': form,
            }
            return render(request, 'person/celery_bulk_create.html', context)

        except Exception as e:
            logger.error(str(e))
            messages.error(request, str(e))
            return redirect('dashboard')

    def post(self, request):
        try:
            # import pdb;pdb.set_trace()
            form = CreateRandomStudentForm(request.POST or None)
            if form.is_valid():
                total = form.cleaned_data.get('total')
                # run process in background, returns response(not full) immediately
                create_random_students.delay(total)
                # run process in foreground, returns response after completing the task
                # create_random_students(total)
                messages.success(request, 'Successfully created total {} students.'.format(total))
                return redirect('person:person_list')
        except Exception as e:
            logger.error(str(e))
            messages.error(request, str(e))
            return redirect('dashboard')
